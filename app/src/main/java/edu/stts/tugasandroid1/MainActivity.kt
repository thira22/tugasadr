package edu.stts.tugasandroid1

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_USER = "EXTRA_USER"

        fun getStartIntent(
                context: Context,
                user: User
        ) = Intent(context, MainActivity::class.java).apply {
            putExtra(EXTRA_USER, user)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val user = intent.getParcelableExtra<User>(EXTRA_USER)

        welcome.text = getString(R.string.welcome) + user.username;
    }
}
